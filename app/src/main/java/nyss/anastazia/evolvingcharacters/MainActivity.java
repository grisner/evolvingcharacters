package nyss.anastazia.evolvingcharacters;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends ActionBarActivity {

    Button[] bButtons = new Button[8];
    Button bStart;
    int[] iButtonID = new int[8];
    String sGoal;
    int iLevel = 2;
    int iMaxClick = 100;
    String TAG = "EvolvingCharacters";
    int iClickCounter;
    Random rand;
    boolean bGameOn=false;

    // A-K
    int iLowerCharacterSpan = 65;
    int iHigherCharacterSpan = 75;

    TextView tLastGen;
    TextView tGoal;
    TextView tGenerations;

    // Cheats
    int iCheat9 = 0;
    int iCheat8 = 0;
    int iCheatEnd = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        tLastGen = (TextView)findViewById(R.id.LastGen);
        tGenerations = (TextView)findViewById (R.id.Generations);
        tGoal = (TextView)findViewById (R.id.Goal);
        rand = new Random();

        tGenerations.setText("");

        bStart = (Button)findViewById (R.id.startButton);

        bButtons[0] = (Button)findViewById(R.id.button0);
        iButtonID[0] = R.id.button0;
        bButtons [1] = (Button)findViewById (R.id.button1);
        iButtonID[1] = R.id.button1;
        bButtons [2] = (Button)findViewById (R.id.button2);
        iButtonID[2] = R.id.button2;
        bButtons [3] = (Button)findViewById (R.id.button3);
        iButtonID[3] = R.id.button3;

        bButtons [4] = (Button)findViewById (R.id.button4);
        iButtonID[4] = R.id.button4;
        bButtons [5] = (Button)findViewById (R.id.button5);
        iButtonID[5] = R.id.button5;
        bButtons [6] = (Button)findViewById (R.id.button6);
        iButtonID[6] = R.id.button6;
        bButtons [7] = (Button)findViewById (R.id.button7);
        iButtonID[7] = R.id.button7;

        // All buttons except Start are disabled
        for(int i=0;i<8; i++)
        {
            bButtons[i].setEnabled(false);
            bButtons[i].setActivated(false);
            bButtons[i].setOnClickListener(new Click());
            bButtons[i].setOnTouchListener(new Touch());
        }


        bStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Button me = (Button)findViewById(v.getId());

                if(bGameOn == false) {
                    Log.d(TAG,"Starting");
                    StartGame(2);
                    tLastGen.setText("Start");
                    me.setText("Started");
                    bGameOn=true;
                }
                else if(iCheat9==2) {
                    iCheat9=0;

                    tLastGen.setText("CHEATER!");
                    Log.d(TAG,"Cheating to level 9");
                    iLevel = 9;
                    StartGame(iLevel);

                }
                else if(iCheat8 == 2) {
                    iCheat8 = 0;
                    tLastGen.setText("CHEATER!");
                    Log.d(TAG,"Cheating to level 8");
                    iLevel=8;
                    StartGame(iLevel);
                }
                else if(iCheatEnd == 2) {
                    iCheatEnd=0;
                    iLevel=8;
                    WIN();
                }
                else {
                    bGameOn=false;
                    GameOver();
                }


            }
        });

    }


    class Click implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Button me = (Button)findViewById(v.getId());

            if(v.getId() == iButtonID[7] && iCheat9<3) {
                iCheat9++;
            }
            else if(v.getId() == iButtonID[7] && iCheat9>2) {
                iCheat9=0;

                if(iCheatEnd == 1) { iCheatEnd++; }
                else {iCheatEnd=0;}
            }
            else if(v.getId() == iButtonID[6] && iCheat8 < 3) {
                if(iCheatEnd<1) { iCheatEnd++; }
                else { iCheatEnd=0; }

                iCheat8++;
            }
            else if(v.getId() == iButtonID[6] && iCheat8 > 2) {
                iCheat8 = 0;
            }

            Iteration(me);
        }
    }


    class Touch implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent e) {
            Button me = (Button)findViewById(v.getId());
            int iOldColor = me.getDrawingCacheBackgroundColor();
            int iNewColor = iOldColor + 0x50000000;
            me.setBackgroundColor(iNewColor);
            return false;
        }

    }

    private int getIntFromColor(int Red, int Green, int Blue)
    {
        Red = (Red << 16) & 0x00FF0000; //Shift red 16-bits and mask out other stuff
        Green = (Green << 8) & 0x0000FF00; //Shift Green 8-bits and mask out other stuff
        Blue = Blue & 0x000000FF; //Mask out anything not blue.

        return 0xFF000000 | Red | Green | Blue;
    }

    void SetColor(Button me)
    {
        String sResult = me.getText().toString();
        int iColourCounter = 0;

        for (int k = 0; k < sGoal.length(); k++)
        {
            int iChar = sResult.charAt(k);
            int iGoalChar = sGoal.charAt(k);

            if (iChar == iGoalChar)
            {
                iColourCounter+=4;
            }
            else if(iChar == iGoalChar+1 || iChar == iGoalChar-1) {
                iColourCounter+=2;
            }
            else if(iChar == iGoalChar+2 || iChar == iGoalChar-2) {
                iColourCounter++;
            }

        }

        int red, green, blue;
        float step = 255/4/sGoal.length();
        green = (int)(iColourCounter*step);
        red = 255-green;
        blue = 0;

        int iColor = getIntFromColor(red, green, blue);
        me.setBackgroundColor(iColor);

        // Tutorial
        String sHelp="";
        if(iLevel == 2) {
            switch(iColourCounter) {
                case 0:
                    sHelp = "0 Kelvin";
                    break;
                case 1:
                    sHelp = "Hard Nitrogen";
                    break;
                case 2:
                    sHelp = "Icy";
                    break;
                case 3:
                    sHelp = "Lukewarm";
                    break;
                case 4:
                    sHelp = "Warmer";
                    break;
                case 5:
                    sHelp = "Hot";
                    break;
                case 6:
                    sHelp = "Hotter";
                    break;
                case 7:
                    sHelp = "Hot as H..avanna";
                    break;
                case 8:
                    sHelp = "Bingo!";
                    break;
            }

            sResult = sHelp + "\n" + sResult;
            me.setText(sResult);
        }
    }

    private void WIN()
    {
        Log.d(TAG, "Handling winning conditions");

        for(int i= 0; i< bButtons.length;i++)
        {
            // 				b.Hide();
            bButtons[i].setEnabled(false);
            bButtons[i].setActivated(false);
            bButtons[i].setBackgroundColor(0x0f999999);
        }

        tLastGen.setText("WINNING is just one way to Play The Game!");

        if (iLevel < 8)
        {
            iLevel++;
            Log.d(TAG,"next: level " +iLevel);
            StartGame(iLevel);
        }
    }

    private void GameOver()
    {
        for(int i= 0; i< bButtons.length;i++)
        {
            // b.Hide();
            bButtons[i].setEnabled(false);
            bButtons[i].setActivated(false);
        }

        bStart.setText("Start");
        tLastGen.setText("Taken out. Take a break, take a walk, take another shot!");

    }

    private void StartGame(int iNewLevel)
    {
        Log.d(TAG,"StartGame level " + iNewLevel);
        // Buttons are populated
        char cCounter = (char)65;

        // We reset the clickcounter for each level
        iClickCounter = 0;

        for(int i= 0; i< 8;i++,cCounter++)
        {
            //b.IsShown = true;
            bButtons[i].setEnabled(true);
            bButtons[i].setActivated(true);

            String sText = "";
            for (int j = 0; j < iNewLevel; j++)
            {
                sText += "" + cCounter;
            }

            bButtons[i].setText(sText);
        }

        // Goal is set
        sGoal = "";

        for (int i = iNewLevel; i > 0; i--)
        {
            int iChar = (rand.nextInt(iHigherCharacterSpan-iLowerCharacterSpan) + iLowerCharacterSpan);
            sGoal += (char)iChar;
        }


        tLastGen.setText("Last gen: ");
        tGoal.setText("FIND\n" + sGoal);

        for (int i = 0; i < 8; i++)
        {
            SetColor (bButtons[i]);
        }
    }


    void Iteration(Button bParent1)
    {
        String sParent1 = bParent1.getText().toString();

        // Tutorial
        if(iLevel == 2) {
            sParent1 = sParent1.substring(sParent1.length()-2,sParent1.length());
        }

        tLastGen.setText("Last\ngen:\n" + sParent1);
        iClickCounter++;
        tGenerations.setText("Generations:\n " + iClickCounter);

        // Win situation?
        Log.d(TAG,sParent1 + ":" + sGoal);
        if (sParent1.equals(sGoal))
        {
            // A WINER IS YOU
            WIN();
        }
        else
        {
            // sParent1 is combined with the other 7 creating 8 new combinations. sParent1 is placed in LastGen
            if (iClickCounter < iMaxClick) {
                for (int i = 0; i < 8; i++) {
                    String sParent2 = bButtons[i].getText().toString();

                    if(iLevel == 2) {
                        sParent2 = sParent2.substring(sParent2.length()-2,sParent2.length());
                    }

                    String sChild = "";
                    int chance;
                    String sResult;

                    for (int j = 0; j < sGoal.length(); j++) {
                        chance = rand.nextInt(2);

                        if (chance == 0) {
                            sChild += sParent1.charAt(j);
                        } else {
                            sChild += sParent2.charAt(j);
                        }
                    }

                    int iChange;
                    char[] cChild = sChild.toCharArray();

                    char cTemp, cTemp2;

                    // Mutation
                    chance = rand.nextInt(5);
                    switch (chance) {
                        case 0:
                            // two characters switch place
                            iChange = rand.nextInt(sGoal.length() - 1);

                            cTemp = cChild[iChange];
                            cTemp2 = cChild[iChange + 1];
                            cChild[iChange] = cTemp2;
                            cChild[iChange + 1] = cTemp;
                            sResult = new String(cChild);

                            break;
                        case 1:
                            // one character is randomized to a new character
                            int iIndex = rand.nextInt(sGoal.length() - 1);
                            int iNewChar = (rand.nextInt(iHigherCharacterSpan - iLowerCharacterSpan) + iLowerCharacterSpan);
                            cChild[iIndex] = (char) iNewChar;

                            sResult = new String(cChild);
                            break;
                        case 2:
                            // one character is changed for ASCII+1 or ASCII-1
                            iIndex = rand.nextInt(sGoal.length());
                            iChange = rand.nextInt(2);

                            if (iChange == 0 && (int) cChild[iIndex] < iHigherCharacterSpan) {
                                cChild[iIndex]++;
                            } else if (iChange == 1 && (int) cChild[iIndex] > iLowerCharacterSpan) {
                                cChild[iIndex]--;
                            }


                            sResult = new String(cChild);
                            break;
                        default:
                            sResult = sChild;
                            break;
                    }

                    // We line break the string if longer than 8 characters
                    // TODO:This still wreaks havoc if level 9 is reached

                    if (sResult.length() > 8) {
                        String sPart1 = sResult.substring(0, (int) (sResult.length() / 2) + 1);
                        String sPart2 = sResult.substring((int) (sResult.length() / 2) + 1, sResult.length());
                        sResult = sPart1 + "\n" + sPart2;
                    }

                    bButtons[i].setText(sResult);

                    // The Colour of the button is updated
                    int iColourCounter = 0;
                    for (int k = 0; k < sGoal.length(); k++) {
                        if (sResult.charAt(k) == sGoal.charAt(k)) {
                            iColourCounter++;
                        }

                    }

                    if (iButtonID[i] != bParent1.getId()) {
                        SetColor(bButtons[i]);
                    }
                }
            }
            else
            {
                GameOver();
            }
        }
        SetColor(bParent1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in androidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);


    }
}
